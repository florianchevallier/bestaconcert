define([
    'jquery',
    'underscore',
    'backbone',
    'text!T/home/templates/register.html',
], function($, _, Backbone, T_Register) {
    return RegisterView = Backbone.View.extend({
        el: "#page",

        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //                           Init + events + render
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


        initialize: function(id) {
            this.render();
        },

        events: {
            "click #btn_register": "register"
        },


        render: function() {
            var template = _.template(T_Register);
            this.$el.html(template);
        },


        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //                           Events
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        register: function(event) {
            var that = this;
            $.ajax({
                url: $.apiURL + "User/AddUser",
                type: 'GET',
                dataType: 'JSON',
                data: {
                    firstName: $("#inputName").val(),
                    lastName: $("#inputPrenom").val(),
                    password: $("#inputPassword1").val(),
                    email: $("#inputEmail1").val(),
                    address: $("#inputAdresse1").val()
                },
                success: function(data) {
                    that.registerSuccess(data);
                }
            });
            return false;
        },

        registerSuccess: function(data) {
            localStorage.setItem("user_email", data.Email);
            localStorage.setItem("user_firstname", data.FirstName);
            localStorage.setItem("user_lastname", data.LastName);
            localStorage.setItem("user_token", data.Token);
            window.location = "#";
            window.location.reload();
        }
    });
});