define([
    'jquery',
    'underscore',
    'backbone',
    'text!T/home/templates/index.html',
    'ddlevelsmenu',
    'carouFredSel',
    'countdown',
    'navgoco',
    'filter',
    'respond',
    'html5shiv'
], function($, _, Backbone, T_Index) {

    return HomeView = Backbone.View.extend({
        el: '#page',


        initialize: function() {
            this.render();
        },

        render: function() {
            var template = _.template(T_Index);
            this.$el.html(template);
        }
    });
});