define([
    'jquery',
    'underscore',
    'backbone',
    'text!T/home/templates/login.html',
], function($, _, Backbone, T_Login) {
    return LoginView = Backbone.View.extend({
        el: "#page",

        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //                           Init + events + render
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


        initialize: function(id) {
            this.render();
        },

        events: {
            "click #signin_btn": "login"
        },


        render: function() {
            var template = _.template(T_Login);
            this.$el.html(template);
        },


        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //                           Events
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        login: function(event) {
            var that = this;
            var apiLink = $.apiURL + "User/SignIn";
            var data = {};
            data.email = $('#inputEmail1').val();
            data.password = $('#inputPassword1').val();
            var that = this;
            $.getJSON(apiLink, data, function(result) {
                that.loginSuccess(result);
            }).fail(function(xhr, textStatus, errorThrown) {
                console.log("failed");
            });
            return false;
        },

        loginSuccess: function(data) {
            // Récupération !
            localStorage.setItem('token', data);
            var apiLink = $.apiURL + "/User/GetUser?token=" + data;
            $.getJSON(apiLink, data, function(result) {
                localStorage.setItem("user_email", result.Email);
                localStorage.setItem("user_firstname", result.FirstName);
                localStorage.setItem("user_lastname", result.LastName);
                localStorage.setItem("user_address", result.Address);
                localStorage.setItem("user_orders", result.Orders);
                if (localStorage.ordersTemp) {
                    localStorage.setItem("user_orderItems", localStorage.ordersTemp)
                    localStorage.ordersTemp = '';
                }

                window.location.reload();
            })
        }
    });
});