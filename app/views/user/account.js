define([
    'jquery',
    'underscore',
    'backbone',
    'text!T/user/templates/account.html'
], function($, _, Backbone, T_Account) {

    return AccountView = Backbone.View.extend({
        el: '#page',


        initialize: function() {
            this.render();
        },

        render: function() {
            var template = _.template(T_Account);
            this.$el.html(template);
        }
    });
});