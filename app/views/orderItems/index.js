define([
    'jquery',
    'underscore',
    'backbone',
    'text!T/user/templates/orderItems.html'
], function($, _, Backbone, T_OrderItems) {

    return OrderItemsView = Backbone.View.extend({
        el: '#page',


        initialize: function() {
            this.render();
        },

        render: function() {
            var template = _.template(T_OrderItems);
            if (localStorage.token) {

            } else {
                var apiLink = $.apiURL + "User/SignIn";
                $.getJSON(apiLink, data, function(result) {
                    that.loginSuccess(result);
                }).fail(function(xhr, textStatus, errorThrown) {
                    console.log("failed");
                });
            }
            this.$el.html(template);
        }


    });
});