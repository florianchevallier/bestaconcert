define([
    'jquery',
    'underscore',
    'backbone',
    'text!T/checkout/templates/index.html'
], function($, _, Backbone, T_Checkout) {

    return CheckoutView = Backbone.View.extend({
        el: '#page',


        initialize: function() {
            if (localStorage.token) {
                this.render();
            } else {
                window.location = "#login";
                $('#shoppingcart').modal('hide');
            }
        },

        events: {
            'click #submit': "submit"
        },

        render: function() {
            var template = _.template(T_Checkout);
            this.$el.html(template);
            $('#shoppingcart').modal('hide');
        },

        submit: function() {
            data = {};
            var month = $('#month').val();
            var year = $('#year').val();
            var num_card = $('#num_card').val();
            data.creditCardNumber = num_card;
            data.expirationDate = month + "/" + year;
            data.userToken = localStorage.token;
            console.log(data);
            var apiUrl = $.apiURLPaiement + "Payment/CheckCreditCard";
            $.getJSON(apiUrl, data, function(json, textStatus) {
                if (json.IsValid == true) {
                    alert("votre commande a bien été prise en compte par nos services.");
                    localStorage.removeItem("order_id");
                    localStorage.removeItem("user_orderItems");
                    localStorage.removeItem("ordersTemp");
                    localStorage.removeItem("price_total");
                    localStorage.removeItem("nb_items");
                    window.location = "#";
                    window.location.reload();
                } else {
                    alert(json.Status)
                }
            });
            return false;
        }


    });
});