define([
    'jquery',
    'underscore',
    'backbone',
    'text!T/items/templates/index.html'
], function($, _, Backbone, T_Index) {

    return ItemsView = Backbone.View.extend({
        el: '#page',


        initialize: function() {
            this.render();
        },

        events: {
            "click .addToCart": "addToCart"
        },
        render: function() {
            var apiLink = $.apiURL + "Concert/GetAll";
            var that = this;
            $.getJSON(apiLink, function(result) {
                var template = _.template(T_Index, {
                    concerts: result
                });
                that.$el.html(template);
            }).fail(function(xhr, textStatus, errorThrown) {
                console.log("failed");
            });
        },

        addToCart: function(event) {
            var concertId = $(event.currentTarget).data("id");
            var quantity = 1;
            var datas = {};
            var price = $(event.currentTarget).data("price");
            var artist = $(event.currentTarget).data("artist");

            var datas = {};
            datas.concertId = concertId;
            datas.quantity = quantity;
            datas.price = price;
            datas.artist = artist;
            if (localStorage.token) {
                var URL = $.apiURL + "Order/AddOrderItem";
                var data = {};
                data.token = localStorage.token
                data.concertId = concertId
                data.quantity = 1;
                $.getJSON(URL, data, function(json, textStatus) {
                    if (json != -1) {
                        if (!localStorage.getItem("order_id"))
                            localStorage.setItem("order_id", json)
                        else
                            localStorage.order_id = json;
                    }
                });
            }
            if (!localStorage.getItem("ordersTemp")) {
                var array = [];
                array.push(datas);
                localStorage.setItem("ordersTemp", JSON.stringify(array));
            } else {
                var array = JSON.parse(localStorage.ordersTemp);
                var flag = false;
                $.each(array, function(index, val) {
                    if (val.concertId == datas.concertId) {
                        val.quantity++;
                        flag = true;
                    }
                });
                if (!flag)
                    array.push(datas)
                localStorage.setItem("ordersTemp", JSON.stringify(array));
            }
            this._updateCart(price);
        },

        _updateCart: function(price) {
            if (!localStorage.getItem("nb_items"))
                localStorage.setItem("nb_items", 1)
            else localStorage.nb_items++;
            if (!localStorage.getItem("price_total"))
                localStorage.setItem("price_total", price)
            else localStorage.price_total = parseInt(localStorage.price_total) + parseInt(price);
            $.gb.updateCart();
        }


    });
});