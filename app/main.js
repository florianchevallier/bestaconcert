require.config({
    // The shim config allows us to configure dependencies for
    // scripts that do not call define() to register a module
    shim: {
        backbone: {
            deps: ['underscore', 'jquery'],
            exports: 'Backbone'
        },
        underscore: {
            exports: '_'
        },
        bootstrap: {
            deps: ['jquery']
        },
        jqueryui: {
            deps: ['jquery']
        },
        ddlevelsmenu: {
            deps: ['jquery']
        },
        carouFredSel: {
            deps: ['jquery', 'ddlevelsmenu']
        },
        countdown: {
            deps: ['jquery', 'carouFredSel']
        },
        navgoco: {
            deps: ['jquery', 'countdown']
        },
        filter: {
            deps: ['jquery', 'navgoco']
        },
        respond: {
            deps: ['jquery']
        },
        html5shiv: {
            deps: ['jquery']
        },
        custom: {
            deps: ['jquery', 'ddlevelsmenu', 'carouFredSel', 'navgoco']
        },
        fonctions: {
            deps: ['jquery']
        }
    },
    paths: {
        jquery: '../assets/js/jquery',
        jqueryui: '../assets/js/jqueryui',
        backbone: '../libs/backbone/backbone.min',
        underscore: '../libs/underscore/underscore.min',
        text: '../libs/require/text',
        bootstrap: '../assets/js/bootstrap.min',
        T: 'views',
        V: 'views',
        ddlevelsmenu: '../assets/js/ddlevelsmenu',
        carouFredSel: '../assets/js/jquery.carouFredSel-6.2.1-packed',
        countdown: '../assets/js/jquery.countdown.min',
        navgoco: '../assets/js/jquery.navgoco.min',
        filter: '../assets/js/filter',
        respond: '../assets/js/respond.min',
        html5shiv: '../assets/js/html5shiv',
        custom: '../assets/js/custom',
        fonctions: 'helpers/fonctions'


    }
});
require([
    'jquery',
    'backbone',
    'router',
    'app',
    'bootstrap',
    'jqueryui',
], function($, Backbone, Workspace, App) {
    /*$.ajaxSetup({
        statusCode: {
            401: function() {
                // Redirec the to the login page.
                window.location.replace('/#login');
            }
        }
    });*/

    $.app.initialize();
});