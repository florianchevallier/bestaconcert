// Filename: models/home
define([
  'underscore',
  'backbone'
], function(_, Backbone){
  var M_Home = Backbone.Model.extend({
		urlRoot: "/home"
	});
  // Return the model for the module
  return new M_Home;
});