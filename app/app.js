    // Filename: app.js
    define([
        'jquery',
        'underscore',
        'backbone',
        'router',
        'text!T/skin.html',
        'V/home/login',
        'helpers/menus',
        'fonctions'
    ], function($, _, Backbone, Workspace, T_Skin, V_Login) {
        $.app = [];

        $.app.initialize = function() {
            $.apiURL = "http://ingesupwebservice.cloudapp.net/API/";
            $.apiURLPaiement = "http://bestconcertpaymentapi.azurewebsites.net/API/";
            var skin = _.template(T_Skin, {
                menus: menus
            });
            $("BODY").html(skin);
            $.assetsURL = "/assets/";
            new Workspace();
            if (!Backbone.History.started) {
                Backbone.history.start();
            } else {
                Backbone.history.stop();
                Backbone.history.start();
            }

            ddlevelsmenu.setup("ddtopmenubar", "topbar");
        }
        return {}
    });