 $.gb || ($.gb = []);
 $.gb.updateCart = function() {
     var nb_items = localStorage.getItem("nb_items");
     var totalPrice = localStorage.getItem("price_total");

     $('.shoppingCartText').html('<i class="fa fa-shopping-cart"></i> ' + nb_items + " concerts - " + totalPrice + '€');
     var buffer = "";
     buffer += '<table class="table table-striped">';
     buffer += '  <thead>';
     buffer += '    <tr>';
     buffer += '      <th>Artist</th>';
     buffer += '      <th>Quantité</th>';
     buffer += '      <th>Prix</th>';
     buffer += '    </tr>';
     buffer += '  </thead>';
     buffer += '  <tbody>';

     var orders = localStorage.user_orderItems ? JSON.parse(localStorage.user_orderItems) : JSON.parse(localStorage.ordersTemp);
     $.each(orders, function(index, val) {
         buffer += '    <tr>';
         buffer += '      <td><a href="#item/' + val.concertId + '">' + val.artist + '</a></td>';
         buffer += '      <td>' + val.quantity + '</td>';
         buffer += '      <td>' + val.price + '€</td>';
         buffer += '    </tr>';
     });
     buffer += '  </tbody>';
     buffer += '</table>';
     $('.shoppingCartBody').html(buffer);
 }