$.fn.serializeObject = function() {
	var o = {};
	var a = this.serializeArray();
	$.each(a, function() {
		if (o[this.name] !== undefined) {
			if (!o[this.name].push) {
				o[this.name] = [o[this.name]];
			}
			o[this.name].push(this.value || '');
		} else {
			o[this.name] = this.value || '';
		}
	});
	return o;
};
$.fn.dateFormat = function(sql, h) { // 2013-01-24 12:05:23   -> 24 Janvier 2013( à 12h05)
	if (sql) {
		var date;
        if (sql == 0 || sql == "0000-00-00 00:00:00" || sql == "00000") return "Jamais";
        var sql = ""+sql;
		var a = sql.split(/[^0-9]/);
		if (a[3] === undefined) {
			return timeConverter(a[0], h);
		}
		date = a[2];
		switch (a[1]) {
			case '01':
				date += ' Janvier ';
				break;
			case '02':
				date += ' Février ';
				break;
			case '03':
				date += ' Mars ';
				break;
			case '04':
				date += ' Avril ';
				break;
			case '05':
				date += ' Mai ';
				break;
			case '06':
				date += ' Juin ';
				break;
			case '07':
				date += ' Juillet ';
				break;
			case '08':
				date += ' Août ';
				break;
			case '09':
				date += ' Septembre ';
				break;
			case '10':
				date += ' Octobre ';
				break;
			case '11':
				date += ' Novembre ';
				break;
			case '12':
				date += ' Décembre ';
				break;
		}
		date += a[0];
		if (h) {
			 date += " à " + a[3] + "h" + a[4]
		}

		return date;
	} else return '--';
}


function timeConverter(UNIX_timestamp, h) {
	var a = new Date(UNIX_timestamp * 1000);
	var months = ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'DécembreDec'];
	var year = a.getFullYear();
	var month = months[a.getMonth()];
	var date = a.getDate();
	var hour = a.getHours();
	var min = a.getMinutes() < 10 ? "0"+a.getMinutes() : a.getMinutes();
    var sec = a.getSeconds() < 10 ? "0"+a.getSeconds() : a.getSeconds();
	if (date) {
		var time = date + ' ' + month + ' ' + year;
		if (h) {
		 	time += ' à ' + hour + ':' + min + ":" + sec;
		 } 
		return time;
	} else {
		return "Non définie";
	}

}