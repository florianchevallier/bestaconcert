 // C'est ici que l'on peut créer le menu
 var menus = [{
     name: "Acueil",
     icon: "icon-dashboard",
     link: "",
     id: "home",
     submenus: null
 }, {
     name: "Locations",
     icon: "icon-text-width",
     link: null,
     id: null,
     submenus: [{
         name: 'Index',
         link: "loc",
         id: "loc",
     }, {
         name: 'Créer une location',
         link: 'loc/new',
         id: "newLoc",
     }]
 }];

 // Ici, on créer les différents fils d'ariane
 var breadcrumbs = new Array();
 breadcrumbs['home'] = [{
     name: 'Home',
     link: "#",
     last: false
 }, {
     name: 'Accueil',
     last: true
 }];

 breadcrumbs['loc'] = [{
     name: 'Locations',
     link: "#",
     last: false
 }, {
     name: 'Accueil',
     last: true
 }];

 breadcrumbs['locNew'] = [{
     name: 'Locations',
     link: "#",
     last: false
 }, {
     name: 'Créer une location',
     last: true
 }];