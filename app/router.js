define([
        'jquery',
        'underscore',
        'backbone',
        'V/home/index',
        'V/home/login',
        'V/home/register',
        'V/user/account',
        'V/items/index',
        'V/checkout/index'
    ],
    function($, _, Backbone, V_Home, V_Login, V_Register, V_Account, V_Items, V_Checkout) {
        return Router = Backbone.Router.extend({
            routes: {
                '': 'home',
                'register': 'register',
                'account': 'account',
                'login': 'login',
                'logout': 'logout',
                'items': 'items',
                'checkout': 'checkout'
            },

            home: function() {
                new V_Home();
            },

            login: function() {
                if (localStorage.getItem("token"))
                    window.location = "#";
                new V_Login();
            },
            logout: function() {
                localStorage.clear();
                window.location = "#";
                window.location.reload();
            },

            register: function() {
                new V_Register();
            },

            account: function() {
                new V_Account();
            },

            items: function() {
                new V_Items();
            },

            checkout: function() {
                new V_Checkout();
            }
        });
    });